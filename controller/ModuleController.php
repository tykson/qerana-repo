<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\controller;

use Qerapp\qbasic\model\module\ModuleService;
use DOMDocument;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  User
  |*****************************************************************************
  |
  | Controller User
  | @author qdevtools,
  | @date 2019-12-12 08:17:10,
  |*****************************************************************************
 */

class ModuleController extends \Qerana\QeranaC
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index()
    {

        $ModuleService = new ModuleService;
        $modules = $ModuleService->getModules();


        echo '<pre>';
        print_r($modules);
        die();

    }

}
