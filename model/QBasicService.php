<?php

/*
 * This file is part of Qbasic
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model;

use Qerapp\qbasic\model\module\ModuleService,
    Qerapp\qbasic\model\modeling\ModelService,
    Qerapp\qbasic\model\modeling\EntityService,
    Qerapp\qbasic\model\controller\ControllerService,
    Qerapp\qbasic\model\view\ViewService;

/**
 * *****************************************************************************
 * Description of DevToolsService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class QBasicService
{

    protected
            $_module,
            $_method,
            $_options;

    public function __construct($service_mod, $method, $options)
    {
        $this->_module = $service_mod;
        $this->_method = $method;
        $this->_options = $options;
    }

    /**
     * -------------------------------------------------------------------------
     * run a service methos
     * -------------------------------------------------------------------------
     */
    public function develop()
    {

        try {
            switch ($this->_module) {

                // module handler
                case 'module':
                    $this->_developModule();
                    break;

                // model handler
                case 'model':
                    $this->_developModel();
                    break;

                // entity handler
                case 'entity':
                    $this->_developEntity();
                    break;

                // module handler
                case 'controller':

                    $this->_developController();
                    break;

                // view
                case 'view':
                    $this->_view();
                    break;
                
                 // relation handler
//                case 'relate':
//                    $this->_relate();
//                    break;

                default:
                    break;
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Develop a new module
     * -------------------------------------------------------------------------
     */
    private function _developModule()
    {
        // check if module name has setted
        if (!array_key_exists('name', $this->_options)) {
            throw new \InvalidArgumentException('You must specified the module name "--name=your_module_name"');
        }

        $ModuleService = new ModuleService();
        $ModuleService->module_name = $this->_options['name'];


        // to create a new module
        if ($this->_method === 'create') {
            $ModuleService->module_type = (isset($this->_options['type'])) ? $this->_options['type'] : 'app';
            $ModuleService->create();
        }

        // to deleted it
        if ($this->_method === 'remove') {
            $ModuleService->remove();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Develop a new model
     * -------------------------------------------------------------------------
     * options available:
     * (required)
     * @param string name name = the name of the model
     * @param string module = module of the model
     * @param string src = the table , xml, any source contain the data structure.
     * (optionals)
     * @param boolean repository, to create or not the repository pattern, true by default
     * @param boolean only_structure, create only structure   
     * 
     */
    private function _developModel()
    {

        try {

            // check if modelname is setted
            if (!array_key_exists('name', $this->_options)) {
                throw new \InvalidArgumentException('set a name for the model "--name=something"');
            }

            // check if module name has setted
            if (!array_key_exists('module', $this->_options)) {
                throw new \InvalidArgumentException('module name is required "--module=your_module_name"');
            }

            $Model = new ModelService;
            $Model->setModule($this->_options['module']);
            $Model->setModel($this->_options['name']);


            // to create a new module
            if ($this->_method === 'create') {
                $Model->create();
            }

            // to deleted it
            if ($this->_method === 'remove') {
                $Model->remove();
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Develop a new entity
     * -------------------------------------------------------------------------
     * 
     */
    private function _developEntity()
    {

        try {

            /**
             * Check params
             * options available
             * --name : name of entity
             * --model: model
             * --src: the origin source, 
             */
            if (!array_key_exists('name', $this->_options)) {
                throw new \InvalidArgumentException('set a name for the model "--name=something"');
            }

            // check if module name has setted
            if (!array_key_exists('model', $this->_options)) {
                throw new \InvalidArgumentException('model name is required "--model=your_model_name"');
            }

            // check if src is setted
            if (!array_key_exists('src', $this->_options)) {
                throw new \InvalidArgumentException('set a source  "--src=something"');
            }


            $EntityService = new EntityService();
            $EntityService->set_entity_name($this->_options['name']);
            $EntityService->setModel($this->_options['model']);
            $EntityService->setSource($this->_options['src']);
            $EntityService->setOrigin();
//            
            // to create a new module
            if ($this->_method === 'create') {
                $EntityService->create();
            }

            // to deleted it
            if ($this->_method === 'remove') {
                $EntityService->remove();
            }

            // to update
            if ($this->_method === 'update') {
                $EntityService->update();
            }
            // to update
            if ($this->_method === 'relate') {
                $EntityService->relate();
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Develop a new controller
     * -------------------------------------------------------------------------
     */
    private function _developController()
    {
        // check if controller name has setted
        if (!array_key_exists('name', $this->_options)) {
            throw new \InvalidArgumentException('You must specified the controller name "--name=your_controller_name"');
        }
        // check if module name has setted
        if (!array_key_exists('module', $this->_options)) {
            throw new \InvalidArgumentException('module name is required "--module=your_module_name"');
        }
        // check if entity has setted
        if (!array_key_exists('entity', $this->_options)) {
            $entity = null;
        } else {
            $entity = $this->_options['entity'];
        }

        $force = (array_key_exists('force', $this->_options)) ? true : false;


        $Controller = new ControllerService($entity, $force);
        $Controller->setModule($this->_options['module']);
        $Controller->set_controller_name($this->_options['name']);
        
        
        
        // to create a new module
        if ($this->_method === 'create') {
            $Controller->create();
        }

        // to deleted it
        if ($this->_method === 'remove') {
            $Controller->remove();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Relate entity
     * -------------------------------------------------------------------------
     */
//    private function _relate()
//    {
//
//        try {
//
//            // to relate a entity
//            if ($this->_method === 'entity') {
//                $RelationService = new RelationService($this->_options);
//                $RelationService->relate();
//            }
//        } catch (\Exception $ex) {
//            echo $ex->getMessage();
//        }
//    }

    /**
     * -------------------------------------------------------------------------
     * Create views
     * -------------------------------------------------------------------------
     */
    private function _view()
    {

        try {

            // create view
            if ($this->_method === 'create') {

                $ViewService = new ViewService($this->_options);
                $ViewService->create();
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

}
