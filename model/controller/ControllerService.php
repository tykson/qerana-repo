<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\controller;

use Qerapp\qbasic\model\controller\ControllerEntity,
    Qerapp\qbasic\model\controller\ControllerXmlMapper AS ControllerMapper,
    Qerapp\qbasic\model\module\ModuleService,
    Qerapp\qbasic\model\modeling\mapper\EntityXmlMapper AS EntityMapper,
    Qerapp\qbasic\model\modeling\entity\attribute\MysqlAttributeMapper;

/**
 * *****************************************************************************
 * Description of ControllerService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class ControllerService
{

    const
            _CONTROLLER_LAYOUT_ = __QERAPPSFOLDER__. 'qbasic/_layouts/tpl_controller_simple.qer',
            _CONTROLLER_SERVICE_LAYOUT_ = __QERAPPSFOLDER__ . 'qbasic/_layouts/tpl_controller_service.qer';

    private
            $_Mapper,
            $_layout,
            $_Entity,
            $_force;
    protected
            $_controller_name,
            $_controller_module;
    public
    /* @object controller enitty */
            $Controller,
            $controller_type,
            $Module;

    public function __construct(string $entity = null, bool $force = true)
    {

        $this->_force = $force;

// check type of controller to create
        if (!is_null($entity)) {
            $this->_layout = self::_CONTROLLER_SERVICE_LAYOUT_;
            $this->_setService($entity);
        } else {
            $this->_layout = self::_CONTROLLER_LAYOUT_;
        }


        $this->_Mapper = new ControllerMapper();
    }

    /**
     * Set controller name
     * @param string $_controller_name
     */
    public function set_controller_name(string $_controller_name)
    {
        $this->_controller_name = ucfirst(filter_var($_controller_name, FILTER_SANITIZE_STRING));
    }

    /**
     * Get Controller
     */
    public function getController()
    {
        
        $this->Controller = $this->_Mapper->getByNameAndModule($this->_controller_name,$this->_controller_module);
    }

    /**
     * Set Module for controller
     * @param string $module_name
     * @throws \RuntimeException
     */
    public function setModule(string $module_name)
    {

        $this->ModuleService = new ModuleService();
        $this->ModuleService->module_name = $module_name;

        // get the module entity
        $this->Module = $this->ModuleService->getModule();

        if (!$this->Module) {
            \QException\Exceptions::ShowException('Controller.Creator', 'Module :' . $module_name . ' dont exists!!');
        }

        $this->_controller_module = $this->Module->name;
    }

    /**
     * -------------------------------------------------------------------------
     * Set service to associate to controller
     * -------------------------------------------------------------------------
     * @param string $entity
     */
    private function _setService(string $entity)
    {

        $EntityMapper = new EntityMapper(new MysqlAttributeMapper);
        
        $this->_Entity = $EntityMapper->findEntity(ucfirst($entity));
        
        if(!$this->_Entity){
            \QException\Exceptions::ShowException('Controller.Creator', 'EntityService :' . $service . ' dont exists!!');
        }
        
        
    }

    /**
     * -------------------------------------------------------------------------
     * Create a new controller if not exists
     * -------------------------------------------------------------------------
     */
    public function create()
    {
        
        $this->getController();
        
        if (!$this->Controller) {


            $Controller = new ControllerEntity();
            $Controller->controller_name = $this->_controller_name;
            $Controller->module = $this->Module->name;
            $Controller->controller_path = $this->Module->module_path . '/controller/' . $this->_controller_name . 'Controller.php';
            $this->_Mapper->save($Controller);

// create file
            $this->_createControllerClass();
        } else {
            if (!$this->_force) {
                \QException\Exceptions::showError('Controller.Creator', 'Controller:' . $this->_controller_name . ', already exists!!');
            } else {
                $this->_createControllerClass();
            }
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Remove controller
     * -------------------------------------------------------------------------
     * @todo , implement to remove all controller components
     */
    public function remove()
    {
        if ($this->Controller) {

            $this->_Mapper->delete($this->Controller);
            unlink($this->Controller->controller_path . '/' . $this->Controller->controller . 'Controller.php');
        } else {
            throw new \Exception('Controller :' . $this->controller_name . ' , dont exists!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Create controller class
     * -------------------------------------------------------------------------
     */
    private function _createControllerClass()
    {

        $this->getController();
        

// first create the file
        \helpers\File::createFile($this->Controller->controller_path . '/' . $this->Controller->controller_name . 'Controller.php');


        $replaces = [
            '[{namespace}]' => $this->Controller->Module->module_namespace . '\\controller',
            '[{service_namespace}]' => '',
            '[{controller_name}]' => $this->_controller_name,
            '[{controller_folder}]' => strtolower($this->_controller_name),
            '[{date}]' => date('Y-m-d H:i:s'),
        ];
        

        if (is_object($this->_Entity)) {
            
            $replaces['[{service_namespace}]'] = $this->_Entity->Model->model_namespace . '\\' . $this->_Entity->entity_name . 'Service';
            $replaces['[{service_name}]'] = $this->_Entity->entity_name . 'Service';
            $replaces['[{entity_name}]'] = $this->_Entity->entity_name;
            $replaces['[{entity_key}]'] = $this->_Entity->entity_key;
        }
        

        $content = strtr(file_get_contents(realpath($this->_layout)), $replaces);
        file_put_contents(realpath($this->Controller->controller_path . '/' . $this->Controller->controller_name . 'Controller.php'), $content);
    }

}
