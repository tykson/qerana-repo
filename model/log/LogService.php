<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\log;

use Qerapp\qbasic\model\log\entity\LogEntity AS LogEntity,
    Qerapp\qbasic\model\log\mapper\LogMapper AS LogMapper,
    Qerapp\qbasic\model\log\mapper\LogMapperInterface,
    Qerapp\qbasic\model\log\repository\LogRepository AS LogRepository;

/*
  |*****************************************************************************
  | LogService
  |*****************************************************************************
  |
  | Service for Entity Log
  | @author TUPA,
  | @date 2019-08-22 19:44:08,
  |*****************************************************************************
 */

class LogService
{

    public
            $log_type,
            $message_log,
            $sw_successfull,
            $id_user;
    protected
            $_LogRepository;

    public function __construct(LogMapperInterface $Mapper = null)
    {
        $MapperRepository = (is_null($Mapper)) ? new LogMapper() : $Mapper;
        $this->_LogRepository = new LogRepository($MapperRepository);
    }

    
    public function getByType(string $type){
        return $this->_LogRepository->findByLog_type($type);
    }
    
    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false)
    {

        $Collection = $this->_LogRepository->findAll();
        return ($json === true) ? \helpers\Utils::parseToJson($Collection) : $Collection;
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false)
    {

        $Entity = $this->_LogRepository->findById($id);
        return ($json === true) ? \helpers\Utils::parseToJson($Entity) : $Entity;
    }

    /**
     * -------------------------------------------------------------------------
     * Register new log
     * -------------------------------------------------------------------------
     */
    public function registerLog()
    {

        $data = [
            'address_ip' => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP),
            'log_type' => $this->log_type,
            'sw_successfull' => $this->sw_successfull,
            'message_log' => $this->message_log,
            'user_agent_log' => filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS),
            'log_timestamp' => date('Y-m-d H:i:s'),
            'id_user' => $this->id_user
        ];


        try {
            $Log = new LogEntity($data);
        } catch (InvalidArgumentException $ex) {
            echo $ex;
        }



        $this->_LogRepository->store($Log);
    }

}
