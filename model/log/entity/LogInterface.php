<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\log\entity;

/*
  |*****************************************************************************
  | INTERFACE  for log
  |*****************************************************************************
  |
  | Entity log
  | @author TUPA,
  | @date 2019-08-22 19:44:08,
  |*****************************************************************************
 */

interface LogInterface
{

//SETTERS

    public function set_id_log(int $id_log): void;

    public function set_id_user(int $id_user): void;

    public function set_log_type(string $log_type): void;

    public function set_message_log(string $message_log): void;

    public function set_log_timestamp(string $log_timestamp): void;

    public function set_sw_successfull(string $sw_successfull): void;

    public function set_user_agent_log(string $user_agent_log): void;

    public function set_address_ip(string $address_ip): void;

//GETTERS

    public function get_id_log();

    public function get_id_user();

    public function get_log_type();

    public function get_message_log();

    public function get_log_timestamp();

    public function get_sw_successfull();

    public function get_user_agent_log();

    public function get_address_ip();
}
