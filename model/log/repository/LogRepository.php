<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\log\repository;

use Qerapp\qbasic\model\log\mapper\LogMapperInterface,
    Qerapp\qbasic\model\log\entity\LogInterface;

/*
  |*****************************************************************************
  | LogRepository
  |*****************************************************************************
  |
  | Repository Log
  | @author TUPA,
  | @date 2019-08-22 19:44:08,
  |*****************************************************************************
 */

class LogRepository implements LogRepositoryInterface
{

    private
            $_LogMapper;

    public function __construct(LogMapperInterface $Mapper)
    {

        $this->_LogMapper = $Mapper;
    }
    
    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_log
     * ------------------------------------------------------------------------- 
     * @param id_log 
     */
    public function findById_log(int $id_log, array $options = [])
    {
        return $this->_LogMapper->findAll(["id_log" => $id_log], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  id_user
     * ------------------------------------------------------------------------- 
     * @param id_user 
     */

    public function findById_user(int $id_user, array $options = [])
    {
        return $this->_LogMapper->findAll(["id_user" => $id_user], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  log_type
     * ------------------------------------------------------------------------- 
     * @param log_type 
     */

    public function findByLog_type($log_type, array $options = [])
    {
        echo '<pre>';
        print_r($log_type);
        die();
        die("aka llego");
        return $this->_LogMapper->findAll(["log_type" => $log_type], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  message_log
     * ------------------------------------------------------------------------- 
     * @param message_log 
     */

    public function findByMessage_log(string $message_log, array $options = [])
    {
        return $this->_LogMapper->findAll(["message_log" => $message_log], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  log_timestamp
     * ------------------------------------------------------------------------- 
     * @param log_timestamp 
     */

    public function findByLog_timestamp(string $log_timestamp, array $options = [])
    {
        return $this->_LogMapper->findAll(["log_timestamp" => $log_timestamp], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  sw_successfull
     * ------------------------------------------------------------------------- 
     * @param sw_successfull 
     */

    public function findBySw_successfull(string $sw_successfull, array $options = [])
    {
        return $this->_LogMapper->findAll(["sw_successfull" => $sw_successfull], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  user_agent_log
     * ------------------------------------------------------------------------- 
     * @param user_agent_log 
     */

    public function findByUser_agent_log(string $user_agent_log, array $options = [])
    {
        return $this->_LogMapper->findAll(["user_agent_log" => $user_agent_log], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  address_ip
     * ------------------------------------------------------------------------- 
     * @param address_ip 
     */

    public function findByAddress_ip(string $address_ip, array $options = [])
    {
        return $this->_LogMapper->findAll(["address_ip" => $address_ip], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all Log
     * -------------------------------------------------------------------------
     * @return LogEntity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        return $this->_LogMapper->findAll($conditions, $options);
    }

    /**
     * find all by type ie: log_type = qemail_12
     * @param array $conditions
     * @return type
     */
    public function findAllByType(array $conditions){
        return $this->_LogMapper->findAll($conditions,['orderby'=>'id_log DESC']);
    }
    
    /**
     * -------------------------------------------------------------------------
     * Save Log
     * -------------------------------------------------------------------------
     * @object $LogEntity
     * @return type
     */
    public function store(LogInterface $LogEntity)
    {
        return $this->_LogMapper->save($LogEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete Log
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_LogMapper->delete($id);
    }

}
