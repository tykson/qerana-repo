<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling;

use Qerapp\qbasic\model\modeling\mapper\EntityXmlMapper AS EntityMapper,
    Qerapp\qbasic\model\modeling\entity\Entity,
    Qerapp\qbasic\model\modeling\mapper\ModelXmlMapper AS ModelMapper;

class EntityService
{

    const
            _ENTITY_LAYOUT_ = __QERAPPSFOLDER__. 'qbasic/_layouts/tpl_entity.qer',
            _INTERFACE_ENTITY_LAYOUT_ = __QERAPPSFOLDER__ . 'qbasic/_layouts/tpl_entity_interface.qer';

    private
    /** @param string, the entity class properties */
            $_properties,
            /** @param string, setters */
            $_setters,
            /** @param string, interface setters */
            $_i_setters,
            /** @param string, getters */
            $_getters,
            /** @param string, interface getters */
            $_i_getters;
    protected
            $_entity_name,
            $_model_name,
            $_ModelMapper,
            $_EntityMapper,
            $_AttributeMapper,
            $_Model;
    public
            $model,
            /** @param  string $name Description name of table of xml file */
            $source,
            /** @param string , if is xml mysql or lite */
            $origin,
            /** @object EntityObject  */
            $Entity;

    public function __construct()
    {
        
    }

    /**
     * Set entity name
     * @param string $entity_name
     */
    public function set_entity_name(string $entity_name)
    {

        $Entity = new Entity;
        $Entity->entity_name = $entity_name;
        $this->_entity_name = $Entity->entity_name;
    }

    /**
     * Set the model
     * @param string $model_name
     */
    public function setModel(string $model_name)
    {

        $ModelService = new ModelService();
        $ModelService->setModel($model_name);

        $this->_Model = $ModelService->Model;

        if (!$this->_Model) {
            throw new \RuntimeException('Model ' . $model_name . ' not exists');
        }
    }

    /**
     * Set the source of entity structure
     * -------------------------------------------------------------------------
     * @throws \InvalidArgumentException
     */
    public function setSource(string $source)
    {

        $this->source = $source;
    }

    /**
     * Set origin
     * @param type $origin
     */
    public function setOrigin($origin = null)
    {
        if (is_null($origin)) {
            $this->origin = 'mysql';
        } else {
            $this->origin = $origin;
        }
    }

    /**
     * Set mapper to use, depending of the origin 
     */
    private function _setAttributeMapper()
    {

        switch ($this->origin) {
            case 'mysql':
                $this->_AttributeMapper = new \Qerapp\qbasic\model\modeling\entity\attribute\MysqlAttributeMapper;
                break;
            case 'xml':
                break;

            default:
                $this->_AttributeMapper = new \Qerapp\qbasic\model\modeling\entity\attribute\MysqlAttributeMapper;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * get Entity
     * -------------------------------------------------------------------------
     */
    private function _getEntity()
    {
        $this->_EntityMapper = new EntityMapper($this->_AttributeMapper);
        $this->Entity = $this->_EntityMapper->findEntity($this->_entity_name);
    }

    /**
     * -------------------------------------------------------------------------
     * Get entity Info, without attribute information
     * -------------------------------------------------------------------------
     * @return type
     */
    public function getEntityInfo()
    {
        $this->_EntityMapper = new EntityMapper();
        return $this->_EntityMapper->findEntity($this->_entity_name);
    }

    /**
     * Create a new entity
     */
    public function create()
    {

        $this->_setAttributeMapper();
        $this->_getEntity();

        if (!$this->Entity) {

            $Entity = new Entity();
            $Entity->entity_name = $this->_entity_name;
            $Entity->model = $this->_Model->model_name;
            $Entity->source = $this->source;
            $Entity->origin = $this->origin;
            $Entity->entity_path = $this->_Model->model_path . '/entity/';
            $this->_EntityMapper->save($Entity);

            $this->_createEntityClass();
        } else {
            \QException\Exceptions::showError('Entity.Creator', 'Entity: ' . $this->_entity_name . ' already exists!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Update a entity
     * -------------------------------------------------------------------------
     */
    public function update()
    {
        $this->_setAttributeMapper();
        $this->_getEntity();

        echo "\n" . ' Check ' . $this->Entity->entity_name . 'Entity';

// first check if exists difference between the source and the Entity attributes

        $this->_checkAndBuildDifferences();

        //print_r($this->_properties);
        // add setters
        if ($this->_properties != '') {

            echo "\n" . ' Differences was found!!, updating entity class ' . " \n";
            $class_entity = file_get_contents($this->Entity->entity_path . '/' . $this->Entity->entity_name . 'Entity.php');

            $atributes = $this->_properties . ',';
            $a = \helpers\File::addNewLineInFile($class_entity, $atributes, '//ATRIBUTES');
            $s = \helpers\File::addNewLineInFile($a, $this->_setters, '//SETTERS');
            $g = \helpers\File::addNewLineInFile($s, $this->_getters, '//GETTERS');
//
            file_put_contents($this->Entity->entity_path . '/' . $this->Entity->entity_name . 'Entity.php', $g);
            echo "\n" . ' Class updated ' . " \n";
        } else {
            echo "\n" . '----No differences was found ----' . " \n";
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Check and find differences between the data source and the entity object
     * attributes, using a reflection class.
     * -------------------------------------------------------------------------
     */
    private function _checkAndBuildDifferences()
    {

        // Create a new entity object
        $entity_namespace = $this->Entity->entity_namespace . '\\' . $this->Entity->entity_name . 'Entity';
        
        $EntityObject = new $entity_namespace;

        // get reflection
        $EntityReflection = new \ReflectionClass(get_class($EntityObject));
        $reflection_properties = $EntityReflection->getProperties();

        // parse each properties
        $array_class_properties = $this->_getPropertiesArray($reflection_properties);
        $array_source_fields = $this->_getPropertiesArray($this->Entity->entity_attributes);

        // get only the differences,
        // always first the array of source fileds, to compare
        $array_differences = array_diff($array_source_fields, $array_class_properties);

        foreach ($array_differences AS $k => $v):

            $this->_parseProperties($this->Entity->entity_attributes[$k]);
            $this->_parseSetter($this->Entity->entity_attributes[$k]);
            $this->_parseGetter($this->Entity->entity_attributes[$k]);
        endforeach;

//
//        echo '<h1>Differences</h1>';
//        print_r($array_differences);
////
//        echo '<h1>Entity properties</h1>';
//        print_r($array_class_properties);
//        echo '<h1>Source fields</h1>';
//        print_r($array_source_fields);
    }

    /**
     * -------------------------------------------------------------------------
     * Get properties objetc to array
     * -------------------------------------------------------------------------
     * @param type $Object
     * @return type
     */
    private function _getPropertiesArray($Object)
    {

        $array_properties = [];

        foreach ($Object AS $Property):

            $array_properties[] = ($Property->name[0] === '_') ? substr($Property->name, 1) : $Property->name;
        endforeach;

        return $array_properties;
    }

    /**
     * -------------------------------------------------------------------------
     * Create a entity class file
     * -------------------------------------------------------------------------
     */
    private function _createEntityClass()
    {
        $this->_getEntity();

        if ($this->Entity) {


            $entity_class = $this->Entity->entity_path . '/' . ucwords($this->Entity->entity_name) . 'Entity.php';
            \helpers\File::createFile($entity_class);


            $entity_interface = $this->Entity->Model->model_path . '/interfaces/' . ucwords($this->Entity->entity_name) . 'Interface.php';
            \helpers\File::createFile($entity_interface);

            $this->_buildEntity();


            $replaces = [
                '[{entity_namespace}]' => $this->Entity->entity_namespace,
                '[{interface_namespace}]' => $this->Entity->Model->model_namespace . '\\interfaces',
                '[{entity_name}]' => $this->Entity->entity_name,
                '[{properties}]' => $this->_properties,
                '[{setters}]' => $this->_setters,
                '[{getters}]' => $this->_getters,
                '[{i_setters}]' => $this->_i_setters,
                '[{i_getters}]' => $this->_i_getters,
                '[{date}]' => date('Y-m-d H:i:s'),
                '[{new_methods}]' => '',
            ];

// content for entity class
            $content = strtr(file_get_contents(realpath(self::_ENTITY_LAYOUT_)), $replaces);
            file_put_contents(realpath($entity_class), $content);

// content for entity interface
            $content_interface = strtr(file_get_contents(realpath(self::_INTERFACE_ENTITY_LAYOUT_)), $replaces);
            file_put_contents(realpath($entity_interface), $content_interface);

// create a mapper class
            $MapperService = new MapperService($this->Entity);
            $MapperService->createMapper();

// create a respository class

            $RepositoryService = new RepositoryService($this->Entity);
            $RepositoryService->createRespository();

            // create the service class
            $Service = new Service($this->Entity);
            $Service->createService();
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Build entity elements
     * -------------------------------------------------------------------------
     */
    private function _buildEntity()
    {


        foreach ($this->Entity->entity_attributes AS $Attribute):

            $this->_parseProperties($Attribute);
            $this->_parseSetter($Attribute);
            $this->_parseGetter($Attribute);

        endforeach;
    }

    /**
     * -------------------------------------------------------------------------
     * Parse attributes
     * -------------------------------------------------------------------------
     * @param type $Attribute
     */
    private function _parseProperties($Attribute)
    {
        $this->_properties .= ($this->_properties != '') ? ", \n" : "";
        $this->_properties .= "/** @var $Attribute->type($Attribute->length), $$Attribute->name  */ \n" . '$_' . $Attribute->name;
    }

    /**
     * -------------------------------------------------------------------------
     * Parse setter
     * -------------------------------------------------------------------------
     * @param object $Attribute
     */
    private function _parseGetter($Attribute)
    {


// check typehint
        $typehint = ($Attribute->type == 'int') ? 'int ' : 'string';
        $name = $Attribute->name;


        switch ($Attribute->type) {


            case 'decimal';

                $getter = '\helpers\Number::toString($this->_' . $name . ');';

                break;

            case 'date';
                $getter = '\helpers\Date::toString($this->_' . $name . ');';
                break;

            case 'datetime';

                $getter = '\helpers\Date::toString($this->_' . $name . ',"d-m-Y H:i:s");';
                break;

            case 'text';

                $getter = ' filter_var($this->_' . $name . ',FILTER_SANITIZE_STRING);';
                break;

            case 'varchar';

                $getter = ' filter_var($this->_' . $name . ',FILTER_SANITIZE_STRING);';
                break;

            case 'int';

                $getter = ' $this->_' . $name . ';';
                break;
            case 'tinyint';

                $getter = ' $this->_' . $name . ';';
                break;
            default:
                $getter = ' $this->_' . $name . ';';
        }



        $this->_getters .= "/** \n"
                . "* ------------------------------------------------------------------------- \n"
                . "* Getter for " . $name . "\n"
                . "* ------------------------------------------------------------------------- \n"
                . "* @return " . $typehint . "  \n"
                . "*/ \n "
                . ' public function get_' . $name
                . '()' . "\n" . "{ \n"
                . ' return ' . $getter . "\n" . '}' . "\n";
        $this->_i_getters .= ' public function get_' . $name
                . '()' . ";\n";
    }

    /**
     * -------------------------------------------------------------------------
     * Parse setter
     * -------------------------------------------------------------------------
     * @param object $Attribute
     */
    private function _parseSetter($Attribute)
    {

// check typehint
        $typehint = ($Attribute->type == 'int') ? 'int ' : 'string';
        $name = $Attribute->name;


        switch ($Attribute->type) {

            case 'varchar';

                $setter = $this->_parseSpecialTypes($Attribute);

                break;

            case 'int';
                $setter = $this->_parseSpecialTypes($Attribute);

                break;
            case 'tinyint';
                $setter = $this->_parseSpecialTypes($Attribute);

                break;
            case 'decimal';

                $setter = '$this->_' . $name . ' = \helpers\Number::toFloat($' . $name . ');';

                break;
            case 'tinyint';

                $setter = '$this->_' . $name . ' = filter_var($' . $name . ',FILTER_SANITIZE_NUMBER_INT);';

                break;

            case 'date';
                $setter = " try { \n"
                        . ' $this->_' . $name . ' = \helpers\Date::toDate($' . $name . ');' . "\n"
                        . ' } catch (\Exception $ex) { ' . " \n"
                        . '     \QException\Exceptions::ShowException("DateError", $ex);' . "\n"
                        . " }"
                        . " ";
                break;

            case 'datetime';

                $setter = " try { \n"
                        . ' $this->_' . $name . ' = \helpers\Date::toDate($' . $name . ');' . "\n"
                        . ' } catch (\Exception $ex) { ' . " \n"
                        . '     \QException\Exceptions::ShowException("DateError", $ex);' . "\n"
                        . " }"
                        . " ";
                break;

            case 'text';

                $setter = '$this->_' . $name . ' = filter_var($' . $name . ',FILTER_SANITIZE_STRING);';
                break;

            default:
                $setter = '$this->_' . $name . ' = filter_var($' . $name . ',FILTER_SANITIZE_STRING);';
        }




        $default_value = ($Attribute->type == 'int') ? 0 : '""';

        $this->_setters .= "/** \n"
                . "* ------------------------------------------------------------------------- \n"
                . "* Setter for " . $name . "\n"
                . "* ------------------------------------------------------------------------- \n"
                . "* @param " . $typehint . " \n"
                . "*/ \n "
                . ' public function set_' . $name
                . '($' . $name . ' = ' . $default_value . '):void' . "\n" . "{ \n"
                . $setter . "\n" . '}';

        $this->_i_setters .= ' public function set_' . $name
                . '(' . $typehint . ' $' . $name . '):void;' . " \n";
    }

    /**
     * -------------------------------------------------------------------------
     * Parse special type of varchars, email, password, etc
     * -------------------------------------------------------------------------
     */
    private function _parseSpecialTypes($Attribute)
    {

        $name = $Attribute->name;
        $length = $Attribute->length;
// email type
        if (strpos($name, 'mail') !== false) {

            $setter = "if(!empty($" . $name . ")) { \n try { \n"
                    . ' $this->_' . $name . ' = \helpers\Utils::checkEmail($' . $name . ');' . "\n"
                    . ' } catch (\Exception $ex) { ' . " \n"
                    . '    \QException\Exceptions::ShowException("' . $this->_entity_name . '.Error.Email", $ex);' . "\n"
                    . " } \n"
                    . "} ";
        }

// dni type
        else if (strpos($name, 'dni') !== false) {

            $setter = ' if(!empty($' . $name . ')) { ' . "\n"
                    . '$' . $name . '_up =  strtoupper($' . $name . ');' . "\n"
                    . " if(\helpers\NifValidator::isValidPersonal($" . $name . "_up)) { \n"
                    . '    $this->_' . $name . ' = $' . $name . '_up;' . "\n"
                    . "} else { \n"
                    . "    \QException\Exceptions::showError('TrabajadorEntity.$name', 'DNI '.$" . $name . ".' Not Valid!!'); \n"
                    . "} \n"
                    . "} ";
        }
// cif type
        else if (strpos($name, 'cif') !== false) {

            $setter = ' if(!empty($' . $name . ')) { ' . "\n"
                    . '$' . $name . '_up =  strtoupper($' . $name . ');' . "\n"
                    . " if(\helpers\NifValidator::isValidEntity($" . $name . "_up)) { \n"
                    . '    $this->_' . $name . ' = $' . $name . '_up;' . "\n"
                    . "} else { \n"
                    . "    \QException\Exceptions::showError('TrabajadorEntity.$name', 'CIF '.$" . $name . ".' Not Valid!!'); \n"
                    . "} \n"
                    . "} ";
        } else if (strpos($name, 'nif') !== false) {

            $setter = ' if(!empty($' . $name . ')) { ' . "\n"
                    . '$' . $name . '_up =  strtoupper($' . $name . ');' . "\n"
                    . " if(\helpers\NifValidator::isValidEntity($" . $name . "_up)) { \n"
                    . '    $this->_' . $name . ' = $' . $name . '_up;' . "\n"
                    . "} else { \n"
                    . "     \QException\Exceptions::showError('TrabajadorEntity.$name', 'NIF '.$" . $name . ".' Not Valid!!'); \n"
                    . "} \n"
                    . "} ";
        } else if (strpos($name, 'nie') !== false) {

            $setter = ' if(!empty($' . $name . ')) { ' . "\n"
                    . '$' . $name . '_up =  strtoupper($' . $name . ');' . "\n"
                    . " if(\helpers\NifValidator::isValidNie($" . $name . "_up)) { \n"
                    . '    $this->_' . $name . ' = $' . $name . '_up;' . "\n"
                    . "} else { \n"
                    . "     \QException\Exceptions::showError('TrabajadorEntity.$name', 'NIE '.$" . $name . ".' Not Valid!!'); \n"
                    . "} \n"
                    . "} ";
        } else if (strpos($name, 'num_ss') !== false) {

            $setter = " if(!empty($" . $name . ")) { \n try { \n"
                    . ' $this->_' . $name . ' = \helpers\Number::checkInt($' . $name . ',' . $length . ');' . "\n"
                    . ' } catch (\Exception $ex) { ' . " \n"
                    . '     \QException\Exceptions::ShowException("' . $this->_entity_name . '.' . $name . '", $ex);' . "\n"
                    . " }\n"
                    . " }\n"
                    . " ";
        } else if (strpos($name, 'social') !== false) {

            $setter = " if(!empty($" . $name . ")) { \n try { \n"
                    . ' $this->_' . $name . ' = \helpers\Number::checkInt($' . $name . ',' . $length . ');' . "\n"
                    . ' } catch (\Exception $ex) { ' . " \n"
                    . '     \QException\Exceptions::ShowException("' . $this->_entity_name . '.' . $name . '", $ex);' . "\n"
                    . " }\n"
                    . " }\n"
                    . " ";
        } else if (strpos($name, 'num_seg') !== false) {

            $setter = " if(!empty($" . $name . ")) { \n try { \n"
                    . ' $this->_' . $name . ' = \helpers\Number::checkInt($' . $name . ',' . $length . ');' . "\n"
                    . ' } catch (\Exception $ex) { ' . " \n"
                    . '     \QException\Exceptions::ShowException("' . $this->_entity_name . '.' . $name . '", $ex);' . "\n"
                    . " }\n"
                    . " }\n"
                    . " ";
        } else if (strpos($name, 'telefono') !== false) {

            $setter = " if(!empty($" . $name . ")) { \n try { \n"
                    . ' $this->_' . $name . ' = \helpers\Number::checkInt($' . $name . ',' . $length . ');' . "\n"
                    . ' } catch (\Exception $ex) { ' . " \n"
                    . '     \QException\Exceptions::ShowException("' . $this->_entity_name . '.' . $name . '", $ex);' . "\n"
                    . " }\n"
                    . " }\n"
                    . " ";
        } else if (strpos($name, 'postal') !== false) {

            $setter = " if(!empty($" . $name . ")) { \n try { \n"
                    . ' $this->_' . $name . ' = \helpers\Number::checkInt($' . $name . ',' . $length . ');' . "\n"
                    . ' } catch (\Exception $ex) { ' . " \n"
                    . '     \QException\Exceptions::ShowException("' . $this->_entity_name . '.' . $name . '", $ex);' . "\n"
                    . " }\n"
                    . " }\n"
                    . " ";
        } else {
            $setter = '$this->_' . $name . ' = filter_var($' . $name . ',FILTER_SANITIZE_STRING);';
        }

        return $setter;
    }

    /**
     * -------------------------------------------------------------------------
     * Remove a model
     * -------------------------------------------------------------------------
     * @throws \RuntimeException
     */
    public function remove()
    {

        $this->_getEntity();

        if ($this->Entity) {


            $path_entity = $this->Entity->entity_path . '/' . $this->Entity->entity_name . 'Entity.php';
            $path_entity_interface = $this->Entity->entity_path . '/' . $this->Entity->entity_name . 'Interface.php';

// delete each file
            unlink($path_entity_interface);
            unlink($path_entity);

//delete Entity
            $this->_EntityMapper->delete($this->Entity);
        } else {
            throw new \RuntimeException('Entity:' . $this->_entity_name . ', dont exists!!');
        }
    }

}
