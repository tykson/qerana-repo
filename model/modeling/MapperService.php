<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling;

use Qerapp\qbasic\model\modeling\entity\Entity;

/**
 * *****************************************************************************
 * Description of MapperService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class MapperService
{

    const
            _MAPPER_LAYOUT_ = __QERAPPSFOLDER__. 'qbasic/_layouts/tpl_sql_mapper.qer',
            _MAPPER_INTERFACE_ = __QERAPPSFOLDER__. 'qbasic/_layouts/tpl_mapper_interface.qer';

    protected
            $_Entity,
            $_mapper_path,
            $_mapper_name,
            $_mapper_namespace;
    
    private
            $_save_entity_code;

    public function __construct(Entity $Entity)
    {
        $this->_Entity = $Entity;
        $this->_mapper_path = realpath($this->_Entity->Model->model_path . '/mapper/');
        $this->_mapper_name = $this->_Entity->entity_name . 'Mapper';
         $this->_mapper_namespace = $this->_Entity->Model->model_namespace . '\\mapper';
        
    }

    /**
     * -------------------------------------------------------------------------
     * Create mapper 
     * -------------------------------------------------------------------------
     */
    public function createMapper()
    {


        $path_mapper_entity = $this->_mapper_path . '/' . $this->_mapper_name . '.php';
        $path_mapper_entity_interface = $this->_Entity->Model->model_path . '/interfaces/' . $this->_mapper_name . 'Interface.php';
        
        // create mapper and mapper interface
        \helpers\File::createFile($path_mapper_entity);
        \helpers\File::createFile($path_mapper_entity_interface);
        
        // create save entity code
        $this->_parseSaveEntity();

        $replaces = [
            '[{mapper_namespace}]' => $this->_mapper_namespace,
            '[{mapper_interface_namespace}]' => $this->_Entity->Model->model_namespace.'\\interfaces',
            '[{entity_namespace}]' => $this->_Entity->entity_namespace.'\\'.$this->_Entity->entity_name.'Entity',
            '[{entity_interface_namespace}]' =>  $this->_Entity->Model->model_namespace.'\\interfaces\\'.$this->_Entity->entity_name.'Interface',
            '[{mapper_name}]' => $this->_mapper_name,
            '[{Entity}]' => $this->_Entity->entity_name,
            '[{save_code}]' => $this->_save_entity_code,
            '[{Entity_key}]' => $this->_Entity->entity_key,
            '[{table_name}]' => $this->_Entity->source,
            '[{date}]' => date('Y-m-d H:i:s'),
        ];
        
        // lets  fill the mapper
        $content_class = strtr(file_get_contents(realpath(self::_MAPPER_LAYOUT_)), $replaces);
        file_put_contents(realpath($path_mapper_entity), $content_class);
        
        // interface mapper
        $content_interface = strtr(file_get_contents(realpath(self::_MAPPER_INTERFACE_)), $replaces);
        file_put_contents(realpath($path_mapper_entity_interface), $content_interface);
    }

    /**
     * -------------------------------------------------------------------------
     * Parse save entity, 
     * 
     * if the table is union table, not have a AI key, the only insert data
     * -------------------------------------------------------------------------
     */
    private function _parseSaveEntity(){
        
        // if has a key ()
        if($this->_Entity->entity_key){
            
            $object_key = $this->_Entity->entity_name.'->'.$this->_Entity->entity_key;
            
            $this->_save_entity_code = 'if (is_null($'.$object_key.')) { '."\n"
                    . '$'.$object_key.' = $this->_Adapter->insert($data);'."\n"
                    . '} else {'."\n"
                    . '$this->_Adapter->update($data, ['."'".$this->_Entity->entity_key."'".' => $'.$object_key.']);'."\n"
                    . '}';
            
            
        }
        
        // if entity table is a many to many relation
        else{
            
            $this->_save_entity_code = '$this->_Adapter->insert($data);';            
        }
        
        
    }
    
}
