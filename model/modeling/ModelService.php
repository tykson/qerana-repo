<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling;

use Qerapp\qbasic\model\modeling\mapper\ModelXmlMapper,
    Qerapp\qbasic\model\modeling\entity\ModelEntity,
    Qerapp\qbasic\model\module\ModuleService;

class ModelService
{

    protected
            $_ModuleService,
            $_ModelMapper,
            $_model_name,
            $_module;
    public
            $Model,
            $Module;

    public function __construct()
    {
        // create model mapper
        $this->_ModelMapper = new ModelXmlMapper(true);

    }
    
    /**
     * Set module name
     * @param string $module
     */
    public function setModule(string $module){
        
        $Module = new \Qerapp\qbasic\model\module\entity\ModuleEntity();
        $Module->name = $module;
        
        $this->_ModuleService = new ModuleService();
        $this->_ModuleService->module_name = $Module->name;
        
        // find module
        $ModuleResult = $this->_ModuleService->getModule();
        
        if($ModuleResult instanceof \Qerapp\qbasic\model\module\entity\ModuleEntity){
            $this->Module = $ModuleResult;
        }
        else{
             \QException\Exceptions::showError('ModelCreator', 'Module:' . $module . ' NOT exists!!');
        }
    }
    
    
    /**
     * Set model name, and search model object
     * @param string $model_name
     */
    public function setModel(string $model_name){
        
        $this->_model_name = $model_name;
        // use entity logic
        $Model = new ModelEntity();
        $Model->model_name = $model_name;
        
        // find in mapper
        $ModelResult = $this->_ModelMapper->findModel($Model->model_name);
        
        if($ModelResult instanceof ModelEntity){
            $this->Model = $ModelResult;
        }
        
    }

  
    /**
     * -------------------------------------------------------------------------
     * Ceate a model
     * -------------------------------------------------------------------------
     * @throws \RuntimeException
     */
    public function create()
    {

        if (!$this->Model) {


            $Model = new ModelEntity();
            $Model->set_model_name($this->_model_name);
            $Model->set_module($this->Module->name);

            // create the model folder structure
            $path_model = realpath($this->Module->module_path . '/model') . '/' . $Model->model_name;


            if (mkdir($path_model, 0777, true)) {

                // entity folde
                mkdir($path_model . '/entity', 0777, true);
                mkdir($path_model . '/mapper', 0777, true);
                mkdir($path_model . '/repository', 0777, true);
                mkdir($path_model . '/interfaces', 0777, true);
                // save model
                $this->_ModelMapper->save($Model);
            }
        } else {
            \QException\Exceptions::showError('ModelCreator', 'Model:' . $this->Model->model_name . ' already exists!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Remove a model
     * -------------------------------------------------------------------------
     * @throws \RuntimeException
     */
    public function remove()
    {

        if ($this->Model) {

            $path_model = realpath($this->Model->model_path);

            if (!empty($path_model)) {
                \helpers\File::deleteFolder($path_model);
                $this->_ModelMapper->delete($this->Model);
            }
        } else {
             \QException\Exceptions::showError('ModelCreator', 'Model:' . $this->Model->model_name . ' NOT exists!!');
        }
    }

}
