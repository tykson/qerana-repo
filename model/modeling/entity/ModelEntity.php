<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling\entity;

use Ada\EntityManager,
    Qerapp\qbasic\model\module\entity\ModuleEntity;

class ModelEntity extends EntityManager
{

    public $Module,
            $model_path,
            $model_namespace;
    protected
            $_id_model,
            $_model_name,
            $_module;

    public function __construct(array $data = [], ModuleEntity $Module = null)
    {
        (!empty($data)) ? $this->populate($data) : '';
        (!is_null($Module)) ? $this->_setModule($Module) : '';
    }

    /**
     * Set Module object
     * @param type $Module
     */
    private function _setModule($Module)
    {
        $this->Module = $Module;
        $this->model_path = realpath($this->Module->module_path . '/model/' . $this->_model_name);
        $this->model_namespace = $this->Module->module_namespace . '\\model\\' . $this->_model_name;
    }

    public function set_id_model($_id_model)
    {
        $this->_id_model = $_id_model;
    }

    /**
     * Set model name, 
     * Only letter and lowercase
     * @param string $_model_name
     * @throws \InvalidArgumentException
     */
    public function set_model_name($_model_name)
    {
        // only letters in model name 
        if (ctype_alpha($_model_name) == 1) {
            $this->_model_name = strtolower($_model_name);
        } else {
            throw new \InvalidArgumentException('Only letters in model name, resolve this, and try again :-)');
        }
    }

    public function set_module($_module)
    {
        $this->_module = $_module;
    }


    public function get_id_model()
    {
        return $this->_id_model;
    }

    public function get_model_name()
    {
        return $this->_model_name;
    }

    public function get_module()
    {
        return $this->_module;
    }

    public function get_modelnamespace()
    {
        return $this->model_namespace;
    }

}
