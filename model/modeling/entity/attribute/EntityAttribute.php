<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace  Qerapp\qbasic\model\modeling\entity\attribute;

use Ada\EntityManager;

/**
 * *****************************************************************************
 * Description of EntityAttribute
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class EntityAttribute extends EntityManager
{

    protected
            $_name,
            $_type,
            $_length,
            $_key,
            $_null,
            $_obs;

    public function __construct(array $data = [])
    {
        (!empty($data)) ? $this->populate($data) : '';
    }

    /*
      |--------------------------------------------------------------------------
      |SETTERS
      |--------------------------------------------------------------------------
      |
     */

    public function set_name($_name)
    {
        $this->_name = $_name;
    }

    public function set_type($_type)
    {
        $this->_type = $_type;
    }

    public function set_length($_length)
    {
        $this->_length = $_length;
    }

    public function set_obs($_obs)
    {
        $this->_obs = $_obs;
    }

    public function set_key($_key)
    {
        $this->_key = $_key;
    }

    public function set_null($_null)
    {
        $this->_null = $_null;
    }

    /*
      |--------------------------------------------------------------------------
      |GETTERS
      |--------------------------------------------------------------------------
      |
     */

    public function get_name()
    {
        return $this->_name;
    }

    public function get_type()
    {
        return $this->_type;
    }

    public function get_length()
    {
        return $this->_length;
    }

    public function get_obs()
    {
        return $this->_obs;
    }

    public function get_key()
    {
        return $this->_key;
    }

    public function get_null()
    {
        return $this->_null;
    }

}
