<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace  Qerapp\qbasic\model\modeling\entity\attribute;

use Ada\adapters\PDOAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qbasic\model\modeling\entity\attribute\EntityAttribute;
    

/**
 * *****************************************************************************
 * Description of MysqlAttributeMapper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class MysqlAttributeMapper extends AdaDataMapper implements AttributeMapperInterface
{

    public function __construct()
    {
        // set mysql adapter
        $Adapter = new PDOAdapter(\Ada\MysqlPDO::singleton());
        parent::__construct($Adapter);
    }

    /**
     * -------------------------------------------------------------------------
     * Create entities attributes
     * -------------------------------------------------------------------------
     * @param type $entity
     */
    public function getEntityAttributes($entity): array
    {
        $attributes = [];
        $rows = $this->_Adapter->selectByQuery('SHOW COLUMNS FROM ' . $entity);

        foreach ($rows AS $attribute):

            $attributes[] = $this->createEntity($attribute);

        endforeach;
        
        return $attributes;
    }

    /**
     * -------------------------------------------------------------------------
     * Create attribute entity
     * -------------------------------------------------------------------------
     * @param array $row
     */
    protected function createEntity(array $row)
    {
        
        $ex = explode('(',$row['Type']);
        $data = [
            'name' => $row['Field'],
            'type' => $ex[0],
            'length' => (isset($ex[1])) ? str_replace(')', '', $ex[1]) : '',
            'key' => $row['Key'],
            'null' => $row['Null'],
            'obs' => $row['Extra']
        ];
        
        
        $Attribute = new EntityAttribute($data);
        return $Attribute;
    }


}
