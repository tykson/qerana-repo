<?php

/*
 * This file is part of Qdevtools
 * Copyright (C) 2019-2020  diemarc  diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\modeling\mapper;

use Ada\adapters\XmlAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qbasic\model\modeling\entity\Entity,
    Qerapp\qbasic\model\modeling\mapper\ModelXmlMapper AS ModelMapper,
    Qerapp\qbasic\model\modeling\entity\attribute\AttributeMapperInterface;

/**
 * *****************************************************************************
 * Description of EntityMapper
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class EntityXmlMapper extends AdaDataMapper
{

    protected
            /** @object model mapper, to find model related */
            $_ModelMapper,
            /** @object attribute mapper */
            $_AttributeMapper;

    public function __construct(AttributeMapperInterface $AttributeMapper = null)
    {
        parent::__construct(new XmlAdapter(__DATA__.'xml/qbasic/entities','entity',false));
        
        $this->_AttributeMapper = $AttributeMapper;
        
        
        $this->_ModelMapper = new ModelMapper();
    }

    /**
     * find by entityl name and model
     * @param string $entity
     * @param string $model
     * @return type
     */
    public function findEntityModel(string $entity, string $model)
    {
        return $this->findOne(['entity_name' => $entity, 'model' => $model]);
    }
    
    /**
     * find entity by name
     * @param string $entity
     * @return type
     */
    public function findEntity(string $entity)
    {
        return $this->findOne(['entity_name' => $entity]);
    }

    /**
     * save a entity
     * @param Entity $Entity
     */
    public function save(Entity $Entity)
    {

        $data = parent::getDataObject($Entity);

        if (is_null($Entity->id_entity)) {
            $Entity->id_entity = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_entity' => $Entity->id_entity]);
        }
    }

    /**
     * Delte a entity
     * @param type $entity
     */
    public function delete($entity)
    {

        if ($entity instanceof Entity) {
            $id = $entity->id_entity;
        } else {
            $id = $entity;
        }
        return $this->_Adapter->delete(['id_entity' => $id]);
    }

    /**
     * Create entity
     * @param array $row
     * @return Entity
     */
    public function createEntity(array $row): Entity
    {
        $Model = $this->_ModelMapper->findModel($row['model']);
        $attributes = (!is_null($this->_AttributeMapper)) ? $this->_AttributeMapper->getEntityAttributes($row['source']): [];
        $Entity = new Entity($row, $Model,$attributes);
     
        return $Entity;
    }

}
