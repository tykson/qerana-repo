<?php

/*
 * Copyright (C) 2020 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\module;

use Qerapp\qbasic\model\module\mapper\ModuleXmlMapper AS ModuleMapper,
    Qerapp\qbasic\model\module\mapper\ModuleMapperInterface,
    Qerapp\qbasic\model\module\repository\ModuleRepository,
    Qerapp\qbasic\model\module\entity\ModuleEntity;

/**
 * *****************************************************************************
 * Description of ModuleService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class ModuleService
{

    public
    /** @entity module */
            $Module,
            /** @object module repository */
            $ModuleRepo,
            /** @var module name */
            $module_name,
            /** @var module type, qerapp o app */
            $module_type;

    public function __construct(ModuleMapperInterface $Mapper = null)
    {
        try {
            $ModuleMapper = (is_null($Mapper)) ? new ModuleMapper : $Mapper;
            $this->ModuleRepo = new ModuleRepository($ModuleMapper);
        } catch (\Exception $ex) {

            \QException\Exceptions::ShowException('XMLCONN', $ex);
        }
    }

    /**
     * Get all modules
     * @return module entities
     */
    public function getModules()
    {

        return $this->ModuleRepo->findAll();
    }

    /**
     * get a module by module name
     * @return type
     */
    public function getModule()
    {

        // create a module entity to use its bussines logic
        $Module = new ModuleEntity();
        $Module->name = $this->module_name;
        
        return $this->ModuleRepo->findByModule($Module->name);
    }

    /**
     * Create a new module if not exists
     * @param string $type , define the target module, app if is an specific module,
     * or querapp if u want to create a querapp app
     */
    public function create()
    {

        // first create module entity
        $this->createModuleEntity();

        // create folder structure
        $this->_createModuleFolders();
    }

    /**
     * Create module Entity and persists
     */
    public function createModuleEntity()
    {

        // first check if module exists
        if (!$this->getModule()) {

            $this->Module = new ModuleEntity();
            $this->Module->name = ($this->module_type === 'qerapp') ? 'q' . $this->module_name : $this->module_name;
            $this->Module->type = (empty($this->module_type)) ? 'app' : $this->module_type;
            $this->Module->layout = '';
            $this->Module->access = 'private';
            $this->Module->style = 'sbadmin2';

            // persists new module entity
            $this->ModuleRepo->store($this->Module);
        } else {

            \QException\Exceptions::showError('ModuleCreator', 'Module:' . $this->module_name . ' already exists!!');
        }
    }

    /**
     * Make module folders
     * for standards apps , this will create (model,controller,view)
     * for qerapps , also create _data and _config folder
     */
    private function _createModuleFolders()
    {

        // Module folder creation, and sub folder for model controller and view 
        if (mkdir($this->Module->module_path, 0777, true)) {

            mkdir($this->Module->module_path . '/model', 0777, true);
            mkdir($this->Module->module_path . '/controller', 0777, true);
            mkdir($this->Module->module_path . '/view', 0777, true);

            // if is qerapp create this folders too
            if ($this->module_type == 'qerapp') {
                mkdir($this->Module->module_path . '/_data', 0777, true);
                mkdir($this->Module->module_path . '/_config', 0777, true);
            }
        }
    }

    /**
     * Remove module
     * @todo , implement to remove all module components
     */
    public function remove()
    {
        
        $this->Module = $this->getModule();
        
        // check if module exists
        if ($this->Module) {

            $this->ModuleRepo->remove($this->Module->name);
            /** @TODO: remove all module components (controllers,entities,etc) */
            // remove all folder structure
            $path_to_remove = realpath($this->Module->module_path);

            if (!empty($path_to_remove)) {
                \helpers\File::deleteFolder($path_to_remove);
            }
        } else {
            \QException\Exceptions::showError('ModuleCreator', 'Module:' . $this->module_name . ' dont exists!!');
            //throw new \Exception('Module :' . $this->module_name . ' , dont exists!!');
        }
    }

}
