<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\module\entity;

use \Ada\EntityManager;

/*
  |*****************************************************************************
  | ENTITY CLASS for Module
  |*****************************************************************************
  |
  | Entity Module
  | @author diemarc,
  | @date 2019-11-09 06:43:37,
  |*****************************************************************************
 */

class ModuleEntity extends EntityManager implements ModuleInterface, \JsonSerializable
{

    protected

    //ATRIBUTES        
            $_id_module,
            /** @var varchar(45), $module  */
            $_name,
            /** @var varchar(45), $layout  */
            $_layout,
            // if is a qerapp o or a app module
            $_type,
            /** @var string if is visible in navmap  */
            $_visible,
            /** @var string , tpl css to use */
            $_style,
            /** @string, if is public or provate */
            $_access;
    //RELATED

    public
            $module_path,
            $module_namespace;

    public function __construct(array $data = [], array $controllers = [])
    {
        $this->populate($data);
        (!empty($controllers)) ? $this->setControllers($controllers) : '';


        //RELATED-LOAD
    }

//SETTERS

    /**
     * -------------------------------------------------------------------------
     * set module name
     * -------------------------------------------------------------------------
     * @param type $_module_name
     */
    public function set_name($_module_name): void
    {

        $this->_name = strtolower($_module_name);
    }

    public function set_layout($_layout): void
    {
        $this->_layout = strtolower($_layout);
    }

    function set_id_module($_id_module)
    {
        $this->_id_module = $_id_module;
    }

    /**
     * -------------------------------------------------------------------------
     * Set type an determine the real path of module
     * -------------------------------------------------------------------------
     * @param type $_type
     */
    public function set_type($_type): void
    {
        $this->_type = strtolower($_type);
        $folder_path = ($this->_type === 'qerapp') ? __QERAPPSFOLDER__ : __APPFOLDER__;
        $this->module_path = $folder_path . '' . $this->_name;
        $this->module_namespace = (($this->_type === 'qerapp') ? 'Qerapp' : 'app') . '\\' . $this->_name;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for type
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_visible($visible): void
    {
        $this->_visible = filter_var($visible, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for access
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_access($access): void
    {
        $this->_access = filter_var($access, FILTER_SANITIZE_STRING);
    }

    public function get_id_module()
    {
        return $this->_id_module;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for type
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_style($style): void
    {
        $this->_style = filter_var($style, FILTER_SANITIZE_STRING);
    }

    public function get_module()
    {
        return $this->_name;
    }

    public function get_layout()
    {
        return $this->_layout;
    }

    public function get_access()
    {
        return $this->_access;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for visible
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_visible()
    {
        return filter_var($this->_visible, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for style
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_style()
    {
        return filter_var($this->_style, FILTER_SANITIZE_STRING);
    }

}
