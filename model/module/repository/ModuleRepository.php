<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\module\repository;

use Qerapp\qbasic\model\module\mapper\ModuleMapperInterface,
    Qerapp\qbasic\model\module\entity\ModuleInterface;

/*
  |*****************************************************************************
  | ModuleRepository
  |*****************************************************************************
  |
  | Repository Module
  | @author TUPA,
  | @date 2019-11-09 06:43:37,
  |*****************************************************************************
 */

class ModuleRepository implements ModuleRepositoryInterface
{

    private
            $_ModuleMapper;

    public function __construct(ModuleMapperInterface $Mapper)
    {

        $this->_ModuleMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all Module
     * -------------------------------------------------------------------------
     * @return ModuleEntity collection
     */
    public function findById(int $id)
    {
        return $this->_ModuleMapper->findOne(['id_module' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all Module
     * -------------------------------------------------------------------------
     * @return ModuleEntity collection
     */
    public function findAll(array $conditions = [], array $options = [])
    {
        return $this->_ModuleMapper->findAll($conditions, $options);
    }
    
    /**
     * -------------------------------------------------------------------------
     * Find all viisbles modules
     * -------------------------------------------------------------------------
     * @return type
     */
    public function findVisibleModules(){
        
        return $this->_ModuleMapper->findAll(['visible' => 1]);
        
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_module
     * ------------------------------------------------------------------------- 
     * @param id_module 
     */
    public function findById_module(int $id_module, array $options = [])
    {
        return $this->_ModuleMapper->findAll(['id_module' => $id_module], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  module
     * ------------------------------------------------------------------------- 
     * @param module 
     */

    public function findByModule(string $module, array $options = [])
    {
        return $this->_ModuleMapper->findOne(['name' => $module], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  layout
     * ------------------------------------------------------------------------- 
     * @param layout 
     */

    public function findByLayout(string $layout, array $options = [])
    {
        return $this->_ModuleMapper->findAll(['layout' => $layout], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save Module
     * -------------------------------------------------------------------------
     * @object $ModuleEntity
     * @return type
     */
    public function store(ModuleInterface $ModuleEntity)
    {
        return $this->_ModuleMapper->save($ModuleEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete Module
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($module_name)
    {
        return $this->_ModuleMapper->delete($module_name);
    }

}
