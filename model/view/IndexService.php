<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\view;

use Qerapp\qbasic\model\view\ViewService;

/**
 * *****************************************************************************
 * Description of IndexService
 * *****************************************************************************
 *
 * @author diemarc
 * *****************************************************************************
 */
class IndexService
{

    const
            _INDEX_LAYOUT_JSON_ = __QERAPPSFOLDER__. 'qbasic/_layouts/view/tpl_index_json.qer',
            _LAYOUT_JAVASCRIPT_ = __QERAPPSFOLDER__. 'qbasic/_layouts/view/js/js.qer',
            _INDEX_LAYOUT_NORMAL_ = __QERAPPSFOLDER__ . 'qbasic/_layouts/view/tpl_index.qer';

    public
// type of index (index,index_json)
            $type = 'json';
    protected
// the titles of the row
            $_row_titles,
            // php code to echo each element
            $_row_data,
            // index location to create
            $_path_to_index,
            // path to js file
            $_path_to_js,
            // template to use, templates must be stored in  /qerana/tupa/src/tpl/view/, with .qer extension
// ex: tpl_index.qer
            $_tpl,
            $_ViewService;

    public function __construct(ViewService $View)
    {


        $this->_ViewService = $View;
        $this->_path_to_index = $this->_ViewService->view_path . '/index_' . strtolower($this->_ViewService->Entity->entity_name) . '.php';
        $this->_path_to_js = __DOCUMENTROOT__ . '/src/js/app/' . strtolower($this->_ViewService->Entity->entity_name) . '.js';
        
       
    }

    /**
     * -------------------------------------------------------------------------
     * Parse attributes to list html
     * -------------------------------------------------------------------------
     */
    private function _parseAtributesToHtml()
    {

        $content = '<?php foreach($' . ucwords($this->_ViewService->main_viewname)
                . 'Collection AS $' . ucwords($this->_ViewService->main_viewname) . '): ?>' . "\n";
        $content .= '<tr>' . "\n";

        $Attributes = $this->_ViewService->attributes;


        foreach ($Attributes AS $AttributeProp):

            $this->_row_titles .= '<td>' . ucwords(str_replace('_', ' ', $AttributeProp->name)) . '</td>' . "\n";
            $content .= '<td><?php echo $' . ucwords($this->_ViewService->main_viewname) . '->'
                    . $AttributeProp->name . '; ?></td>' . "\n";

        endforeach;
        $content .= '</tr>' . "\n";
        $content .= '<?php endforeach;?>' . "\n";
        $this->_row_data = $content;
    }

    /**
     * -------------------------------------------------------------------------
     * Parse attributes to list json
     * -------------------------------------------------------------------------
     */
    private function _parseAtributesToJson()
    {

        $content = '';

        $Attributes = $this->_ViewService->Entity->entity_attributes;

        foreach ($Attributes AS $AttributeProp):

            $this->_row_titles .= '<td>' . ucwords(str_replace('_', ' ', $AttributeProp->name)) . '</td>' . "\n";
            $content .= 'list_' . $this->_ViewService->Entity->entity_name . ' +=' . "'<td>'+" . $this->_ViewService->Entity->entity_name . '.' . $AttributeProp->name . '+' . "'</td>'; \n";

        endforeach;
        $this->_row_data = $content;
    }

    /**
     * -------------------------------------------------------------------------
     * Create index view
     * -------------------------------------------------------------------------
     */
    public function create()
    {

         
        
        // create index and js file
        if (!is_file($this->_path_to_index)) {
            \helpers\File::createFile($this->_path_to_index);
        }

        if (!is_file($this->_path_to_js)) {
            \helpers\File::createFile($this->_path_to_js);
        }
        
    

        if ($this->type == 'standard') {
            $this->_tpl = 'tpl_index.qer';
            $this->_parseAtributesToHtml();
        } else if ($this->type == 'json') {
            //  echo 'json';
            $this->_tpl = 'tpl_index_json.qer';
            $this->_parseAtributesToJson();
        }
    

        $replaces = [
            '[{title}]' => ucfirst($this->_ViewService->Entity->entity_name) . ' List',
            '[{entity}]' => $this->_ViewService->Entity->entity_name,
            '[{module}]' => $this->_ViewService->Entity->Model->Module->name,
            '[{key}]' => $this->_ViewService->Entity->entity_key,
            '[{table_titles}]' => $this->_row_titles,
            '[{table_content}]' => $this->_row_data
        ];
        
        
// create the index view
        $content = strtr(file_get_contents(realpath(self::_INDEX_LAYOUT_JSON_)), $replaces);
        file_put_contents(realpath($this->_path_to_index), $content);

//// create the java script
        $content_js = strtr(file_get_contents(realpath(self::_LAYOUT_JAVASCRIPT_)), $replaces);
        file_put_contents(realpath($this->_path_to_js), $content_js);
    }

}
