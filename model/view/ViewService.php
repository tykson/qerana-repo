<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qbasic\model\view;

use Qerapp\qbasic\model\modeling\mapper\EntityXmlMapper AS EntityMapper,
    Qerapp\qbasic\model\modeling\entity\attribute\MysqlAttributeMapper;

/**
 * *****************************************************************************
 * ViewService
 * *****************************************************************************
 *  Manage views creation for a Entity object
 * @author diemarc
 * *****************************************************************************
 */
class ViewService
{

    protected
            $_entity_name,
            $_options = [];
    public

    /** @object , Entity object to create views */
            $Entity,
            /** @param string, view to create (index,add,edit,etc) */
            $view,
            $view_path;

    public function __construct($options)
    {

        $this->_options = $options;
        $this->_initParams();

        // get entity info
        $EntityMapper = new EntityMapper(new MysqlAttributeMapper());
        $this->Entity = $EntityMapper->findEntity($this->_entity_name);


        if (!$this->Entity) {
            \QException\Exceptions::showError('View.Creator', 'Entity ' . $this->_entity_name . ', dont exists!!');
        }
        $this->set_view_path($this->Entity->Model->Module->module_path . '/view');
    }

    /**
     * -------------------------------------------------------------------------
     * Initialize params
     * -------------------------------------------------------------------------
     */
    private function _initParams()
    {

        $view_to_create = (!isset($this->_options['view'])) ? 'all' : $this->_options['view'];
        $this->set_view($view_to_create);
        $this->set_entity_name($this->_options['entity']);
    }

    /**
     * -------------------------------------------------------------------------
     * Create views
     * -------------------------------------------------------------------------
     */
    public function create()
    {

        switch ($this->view):

            // index file
            case 'index';

                $IndexService = new IndexService($this);
                $IndexService->create();
                break;

            // detail file
            case 'detail';
                $DetailService = new DetailService($this, 'DEFAULT');
                $DetailService->create();
                break;
            // detail JSON file
            case 'detail_json';
                $DetailService = new DetailService($this, 'JSON');
                $DetailService->create();
                break;

            // add form
            case 'add';
                $AddFormService = new FormService($this, 'add');
                $AddFormService->create();
                break;

            // edit form
            case 'edit';
                $EditFormService = new FormService($this, 'edit');
                $EditFormService->create();
                break;

            // create all
            case 'all';

                $IndexService = new IndexService($this);
                $IndexService->create();
                $DetailService = new DetailService($this, 'DEFAULT');
                $DetailService->create();
                $AddFormService = new FormService($this, 'add');
                $AddFormService->create();
                $EditFormService = new FormService($this, 'edit');
                $EditFormService->create();



        endswitch;
    }

    /**
     * -------------------------------------------------------------------------
     * Start views creation
     * -------------------------------------------------------------------------
     */
//    public function createViewStructure()
//    {
//
//   
//        // first create view module folder
//        if (empty(realpath($this->_module_full_path))) {
//            echo 'View model structure dont exists, creating...OK' . " \n";
//            mkdir($this->_module_full_path, 0777, true);
//        }
//
//        // index view
//        if (empty(realpath($this->_index_filename))) {
//            \helpers\File::createFile($this->_index_filename);
//        }
//        // add view
//        if (empty(realpath($this->add_form_filename))) {
//            \helpers\File::createFile($this->add_form_filename);
//        }
//        // edit view
//        if (empty(realpath($this->edit_form_filename))) {
//            \helpers\File::createFile($this->edit_form_filename);
//        }
//        // detail view
//        if (empty(realpath($this->detail_filename))) {
//            \helpers\File::createFile($this->detail_filename);
//        }
//        // js
//        if (empty(realpath($this->js_file))) {
//            \helpers\File::createFile($this->js_file);
//        }
//        
//        
// 
//        
//    }

    public function set_view($_view)
    {
        $this->view = $_view;
    }

    public function set_entity_name($_entity_name)
    {
        $this->_entity_name = ucfirst($_entity_name);
    }

    /**
     * -------------------------------------------------------------------------
     * Set path view
     * -------------------------------------------------------------------------
     * @param type $_view_path
     */
    public function set_view_path($_view_path)
    {

        $path_view = realpath($_view_path . '/' . strtolower($this->_entity_name));

        if (empty($path_view)) {

            mkdir(realpath($_view_path) . '/' . strtolower($this->_entity_name), 0777, true);
        }


        $this->view_path = $path_view;
    }

    public function get_view()
    {
        return $this->view;
    }

    public function get_view_path()
    {
        return $this->view_path;
    }

}
